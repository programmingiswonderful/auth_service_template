module gitlab.com/programmingiswonderful/auth_service_template

go 1.20

require (
	github.com/jackc/pgconn v1.14.0 // indirect
	github.com/jackc/pgx/v4 v4.0.0-pre1.0.20190824185557-6972a5742186
)

require (
	github.com/joho/godotenv v1.4.0
	github.com/spf13/cast v1.5.0
	github.com/streamingfast/logging v0.0.0-20221209193439-bff11742bf4c
	go.uber.org/zap v1.21.0
	golang.org/x/crypto v0.6.0
)

require (
	github.com/blendle/zapdriver v1.3.1 // indirect
	github.com/jackc/chunkreader/v2 v2.0.1 // indirect
	github.com/jackc/pgio v1.0.0 // indirect
	github.com/jackc/pgpassfile v1.0.0 // indirect
	github.com/jackc/pgproto3/v2 v2.3.2 // indirect
	github.com/jackc/pgservicefile v0.0.0-20221227161230-091c0ba34f0a // indirect
	github.com/jackc/pgtype v0.0.0-20190828014616-a8802b16cc59 // indirect
	github.com/logrusorgru/aurora v2.0.3+incompatible // indirect
	github.com/mitchellh/go-testing-interface v1.14.1 // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/sys v0.5.0 // indirect
	golang.org/x/term v0.5.0 // indirect
	golang.org/x/text v0.7.0 // indirect
	golang.org/x/xerrors v0.0.0-20200804184101-5ec99f83aff1 // indirect
)
