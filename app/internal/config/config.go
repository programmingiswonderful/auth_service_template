package config

import (
	"fmt"
	"os"

	"github.com/joho/godotenv"
	"github.com/spf13/cast"
)

const (
	DebugMode   = "debug"
	TestMode    = "test"
	ReleaseMode = "release"
)

type Config struct {
	App ApplicationConfig

	Listen ListenConfig

	Postgres PostgresConfig

	OffsetLimit OffsetLimitConfig

	SecretKey string

	Passcode PasscodeConfig

	SettingsService SettingsServiceConfig
}

type ListenConfig struct {
	HTTPPort   string
	HTTPScheme string
	Host       string
	GRPCPort   string
}
type ApplicationConfig struct {
	ServiceName string
	Environment string
	Version     string
	LogLevel    string
}
type PostgresConfig struct {
	Host           string
	Port           int
	User           string
	Password       string
	Database       string
	MaxConnections int32
}
type OffsetLimitConfig struct {
	DefaultOffset string
	DefaultLimit  string
}
type PasscodeConfig struct {
	Pool   string
	Length int
}
type SettingsServiceConfig struct {
	Host     string
	GRPCPort string
}

func Load() Config {
	if err := godotenv.Load(); err != nil {
		fmt.Println("No .env file found")
	}

	config := Config{}

	config.App.ServiceName = cast.ToString(getOrReturnDefaultValue("SERVICE_NAME", "auth_service"))
	config.App.Environment = cast.ToString(getOrReturnDefaultValue("ENVIRONMENT", DebugMode))
	config.App.Version = cast.ToString(getOrReturnDefaultValue("VERSION", "1.0"))
	config.App.LogLevel = cast.ToString(getOrReturnDefaultValue("LOG_LEVEL", "trace"))

	config.Listen.HTTPPort = cast.ToString(getOrReturnDefaultValue("HTTP_PORT", ":8080"))
	config.Listen.HTTPScheme = cast.ToString(getOrReturnDefaultValue("HTTP_SCHEME", "http"))

	config.Postgres.Host = cast.ToString(getOrReturnDefaultValue("POSTGRES_HOST", "0.0.0.0"))
	config.Postgres.Port = cast.ToInt(getOrReturnDefaultValue("POSTGRES_PORT", 5432))
	config.Postgres.User = cast.ToString(getOrReturnDefaultValue("POSTGRES_USER", "postgres"))
	config.Postgres.Password = cast.ToString(getOrReturnDefaultValue("POSTGRES_PASSWORD", "your_db_password"))
	config.Postgres.Database = cast.ToString(getOrReturnDefaultValue("POSTGRES_DATABASE", config.App.ServiceName))
	config.Postgres.MaxConnections = cast.ToInt32(getOrReturnDefaultValue("POSTGRES_MAX_CONNECTIONS", 30))

	config.OffsetLimit.DefaultOffset = cast.ToString(getOrReturnDefaultValue("DEFAULT_OFFSET", "0"))
	config.OffsetLimit.DefaultLimit = cast.ToString(getOrReturnDefaultValue("DEFAULT_LIMIT", "10"))

	config.SecretKey = cast.ToString(getOrReturnDefaultValue("SECRET_KEY", "Here$houldBe$ome$ecretKey"))

	config.Passcode.Pool = cast.ToString(getOrReturnDefaultValue("PASSCODE_POOL", "0123456789"))
	config.Passcode.Length = cast.ToInt(getOrReturnDefaultValue("PASSCODE_LENGTH", "6"))

	config.SettingsService.Host = cast.ToString(getOrReturnDefaultValue("SETTINGS_SERVICE_HOST", "0.0.0.0"))
	config.SettingsService.GRPCPort = cast.ToString(getOrReturnDefaultValue("SETTINGS_GRPC_PORT", ":9101"))

	config.Listen.Host = cast.ToString(getOrReturnDefaultValue("AUTH_SERVICE_HOST", "0.0.0.0"))
	config.Listen.GRPCPort = cast.ToString(getOrReturnDefaultValue("AUTH_GRPC_PORT", ":9103"))

	return config
}

func getOrReturnDefaultValue(key string, defaultValue interface{}) interface{} {
	val, exists := os.LookupEnv(key)

	if exists {
		return val
	}

	return defaultValue
}
